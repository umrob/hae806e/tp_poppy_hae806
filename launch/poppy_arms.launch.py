import launch
import launch_ros
import xacro


def generate_launch_description():
    poppy_control_node = launch_ros.actions.Node(
        package='tp_poppy_hae806',
        executable='poppy_control',
        output='screen',
        parameters=[
            {'type': 'arms'}]
    )

    return launch.LaunchDescription([
        poppy_control_node
    ])
