from setuptools import setup
from glob import glob
import os

package_name = 'tp_poppy_hae806'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Benjamin Navarro',
    maintainer_email='navarro.benjamin13@gmail.com',
    description='Base project for practical work on Poppy in HAE806',
    license='MIT',
    entry_points={
        'console_scripts': [
            'poppy_control = tp_poppy_hae806.poppy_control:main'
        ],
    },
)
